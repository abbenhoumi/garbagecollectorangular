import { Component, OnInit } from '@angular/core';
import { PoubelleService } from '../services/poubelle.service';
import { Poubelle } from '../poubelle/poubelle';
import { Subscription } from 'rxjs';
import * as Mapboxgl from 'mapbox-gl';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  map: Mapboxgl.Map; // je veux utiliser Mapboxgl dans ce component 
  rootHttp: string = "http://localhost:8081/poubelles/";
  poubelles: Poubelle[];
  poubelleSubscription: Subscription;
  affichemaps = true;

  constructor(private poubelleService: PoubelleService) { 

  }

  
  ngOnInit(): void {
    (Mapboxgl as any).accessToken = environment.mapboxkey;
    this.map = new Mapboxgl.Map({
      container: 'map-mapbox',
      style: 'mapbox://styles/mapbox/dark-v10',
      center: [3.0850959,45.7795433],
      zoom: 14
      });

      let p1: Poubelle = new Poubelle(45.7785134, 3.0859659, 30);
      let p2: Poubelle = new Poubelle(45.7786024, 3.0843234, 90);
      this.createMarker(p1);
      this.createMarker(p2);

      this.poubelleSubscription = this.poubelleService.poubellesSubject.subscribe(
        (poubelles: Poubelle[]) => {
          this.poubelles = poubelles;
          this.poubelles.forEach((poubelle) => this.createMarker(poubelle));
          console.log("Resultat backend : " , poubelles);
          console.log(this.poubelles);
        }
      );

      this.poubelleService.emitPoubellesSubject();
      this.poubelleService.getAllPoubelles();
      

  }

 

  createMarker(poubelle: Poubelle){  
    var el = document.createElement('div');
    el.className = 'marker';
    el.style.backgroundImage = poubelle.capacite > 50 ? 'url("../assets/img/full-garbage.png")' : 'url("../assets/img/garbage.png")';
    el.style.width = '16px';
    el.style.height = '16px';
    console.log('el=', el);

    var marker = new Mapboxgl.Marker(el)
      .setLngLat( [poubelle.longitude,poubelle.latitude] )
      .setPopup(new Mapboxgl.Popup({ offset: 25 })
      .setHTML('<h3>' + '</h3><p>' + ' Contenu : ' + poubelle.capacite + '</p>'))
      .addTo(this.map);


    console.log('adding static marker ', marker);
    console.log('map=', this.map);
  }

  ngOnDestroy() {
    this.poubelleSubscription.unsubscribe();
  }
  
}
