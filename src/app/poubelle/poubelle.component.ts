import { Component, OnInit } from '@angular/core';
import {PoubelleService} from '../services/poubelle.service';

@Component({
  selector: 'app-poubelle',
  templateUrl: './poubelle.component.html',
  styleUrls: ['./poubelle.component.css']
})
export class PoubelleComponent implements OnInit {
  
  constructor(private poubelleService: PoubelleService) { }

  ngOnInit() {
  }

}
