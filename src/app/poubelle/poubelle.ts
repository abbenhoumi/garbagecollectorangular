export class Poubelle {
    id: number;
    longitude: number;
    latitude: number;
    capacite: number;
    static last_id:number = 5;

    constructor(latitude:number, longitude: number, capacite: number) {
      this.latitude = latitude;
      this.longitude = longitude;
      this.capacite = capacite;
      this.id = Poubelle.last_id++;
    }
  }