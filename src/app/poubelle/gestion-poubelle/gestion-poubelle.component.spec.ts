import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionPoubelleComponent } from './gestion-poubelle.component';

describe('GestionPoubelleComponent', () => {
  let component: GestionPoubelleComponent;
  let fixture: ComponentFixture<GestionPoubelleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionPoubelleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionPoubelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
