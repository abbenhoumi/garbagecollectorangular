import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { PoubelleComponent } from './poubelle/poubelle.component';
import { GestionPoubelleComponent } from './poubelle/gestion-poubelle/gestion-poubelle.component';
import { Routes, RouterModule } from '@angular/router';
import { MapsComponent } from './maps/maps.component';

const appRoutes: Routes = [
  { path: 'gestion-poubelles', component: GestionPoubelleComponent },
  { path: 'maps-poubelles', component: MapsComponent },
  { path: '', component: MapsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PoubelleComponent,
    GestionPoubelleComponent,
    MapsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})


export class AppModule { }
