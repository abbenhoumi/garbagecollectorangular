import {Poubelle} from '../poubelle/poubelle';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root',})
export class PoubelleService {
rootHttp: string = "http://localhost:8081/poubelles/";
poubelles: Poubelle[] = [];

poubellesSubject = new Subject<Poubelle[]>();

constructor(private httpClient: HttpClient) {}

emitPoubellesSubject() {
    this.poubellesSubject.next(this.poubelles.slice());
}


//Récuperation des poubelles
getAllPoubelles() {
    this.httpClient.get<Poubelle[]>(this.rootHttp).subscribe(
        (response) => {
            this.poubelles = response;
            this.emitPoubellesSubject();
        },
        (error) => {
            console.log("Error : " + error);
        }
    );
}
    
postPoubelles() {
    this.httpClient.post(this.rootHttp, this.poubelles).subscribe(
        () => {
            console.log("Enregistrement des poubelles avec succès !");
        },
        (error) => {
            console.log("Erreur lors de l'enregistrement des poubelles : " + error);
        }
    );
}

}